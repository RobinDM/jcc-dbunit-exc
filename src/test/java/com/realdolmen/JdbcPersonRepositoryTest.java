package com.realdolmen;

import org.dbunit.Assertion;
import org.dbunit.JdbcBasedDBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;

import java.io.FileInputStream;
import java.sql.Date;
import java.util.Calendar;

/**
 * When using the utility base classes, you need to name your test functions textXxx, the annotation @Test is not found.
 */
public class JdbcPersonRepositoryTest extends JdbcBasedDBTestCase {

    public void testFind() {
        Person person = new JdbcPersonRepository().find(1);
        System.out.println("Person: " + person);
        assertEquals(
                "Jimi",
                person.getFirstName()
        );
        assertEquals(
                "Hendrix",
                person.getLastName()
        );
        assertEquals(
                "5th Avenue",
                person.getAddress()
                      .getStreet()
        );
        assertEquals(
                "2500",
                person.getAddress()
                      .getNumber()
        );
        assertEquals(
                "New York",
                person.getAddress()
                      .getCity()
                      .getName()
        );
        assertEquals(
                "36000",
                person.getAddress()
                      .getCity()
                      .getPostalCode()
        );
        // add birthdate check
    }

    public void testRemove() {
        JdbcPersonRepository jdbcPersonRepository = new JdbcPersonRepository();
        Person jimi = jdbcPersonRepository.find(1);
        jdbcPersonRepository.remove(jimi);
        try {
            IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("lite-after-delete.xml"));
//            IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("lite.xml"));
            IDataSet actualDataSet = getConnection().createDataSet();
            Assertion.assertEquals(
                    actualDataSet.getTable("person"),
                    expectedDataSet.getTable("person")
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void testSave() {
        Person me = new Person(
                "Robin",
                "De Mol",
                new Date(Calendar.getInstance()
                                 .getTimeInMillis()),
                new Address(
                        "Nederkouter",
                        "21",
                        new City(
                                "Eke",
                                "9810"
                        )
                )
        );
        JdbcPersonRepository jdbcPersonRepository = new JdbcPersonRepository();
        jdbcPersonRepository.save(me);
        assertNotNull(me.getId());
        Person amIActuallyThere = jdbcPersonRepository.find(me.getId());
        // TODO: should compare entire person object
        assertEquals(
                me.getFirstName(),
                amIActuallyThere.getFirstName()
        );
    }

    public void testUpdate() {
        fail("not yet implemented");
    }

    @Override
    protected IDataSet getDataSet() throws
                                    Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("lite.xml"));
    }

    @Override
    protected String getConnectionUrl() {
        return "jdbc:mysql://localhost:3306/dbunitppl?user=dev&password=test";
    }

    @Override
    protected String getDriverClass() {
        return "com.mysql.jdbc.Driver";
    }
}