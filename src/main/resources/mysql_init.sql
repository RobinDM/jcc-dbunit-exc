create table city(
    id int auto_increment primary key,
    name text not null,
    postalCode text not null
);
create table address(
    id int auto_increment primary key,
    street text not null,
    number text not null,
    city_id int not null,
    foreign key (city_id) references city(id)
);
create table person(
    id int auto_increment primary key,
    firstName text not null,
    lastName text not null,
    birthDate date not null,
    address_id int not null,
    foreign key (address_id) references address(id)
);
