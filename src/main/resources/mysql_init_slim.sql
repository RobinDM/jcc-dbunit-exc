     CREATE TABLE person (
     id int(11) NOT NULL AUTO_INCREMENT primary key,
     firstName varchar(255) DEFAULT NULL,
     lastName varchar(255) DEFAULT NULL,
     street varchar(255) DEFAULT NULL,
     number varchar(255) DEFAULT NULL,
     city varchar(255) DEFAULT NULL,
     postalcode varchar(255) DEFAULT NULL,
     birthDate date DEFAULT NULL
     )
