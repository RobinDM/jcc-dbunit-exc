package com.realdolmen;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class XMLDataSetBuilder {
    public static void main(String[] args) throws
                                           SQLException,
                                           DatabaseUnitException,
                                           IOException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/dbunitppl?user=dev&password=test");
        DatabaseConnection dbConnection = new DatabaseConnection(connection);
        IDataSet dataSet = dbConnection.createDataSet();
        FlatXmlDataSet.write(dataSet, new FileOutputStream("full.xml"));
    }
}
