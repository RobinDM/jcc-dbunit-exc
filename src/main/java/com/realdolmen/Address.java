package com.realdolmen;

import java.util.Objects;

public class Address {
    private String street;
    private String number;
    private City city;

    public Address(String street, String number, City city) {
        this.street = street;
        this.number = number;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public String getNumber() {
        return number;
    }

    public City getCity() {
        return city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Address address = (Address) o;
        return Objects.equals(
                street,
                address.street
        ) && Objects.equals(
                number,
                address.number
        ) && Objects.equals(
                city,
                address.city
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                street,
                number,
                city
        );
    }
}
