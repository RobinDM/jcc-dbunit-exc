package com.realdolmen;

import java.util.Date;
import java.util.Objects;

public class Person {
    private Integer id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Address address;

    public Person(String firstName, String lastName, Date birthDate, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.address = address;
    }

    void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public Address getAddress() {
        return address;
    }

    public String name() {
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Person person = (Person) o;
        return Objects.equals(
                firstName,
                person.firstName
        ) && Objects.equals(
                lastName,
                person.lastName
        ) && Objects.equals(
                birthDate,
                person.birthDate
        ) && Objects.equals(
                address,
                person.address
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                firstName,
                lastName,
                birthDate,
                address
        );
    }
}
